<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contribute', function(){
  return view('ppe.contribute');
})->name('contribute-ppe');

Route::get('/need-ppe', function(){
  return view('ppe.need');
})->name('need-ppe');

Route::post('/request-ppe', 'RequestsController@create')->name('request-ppe');
Route::get('/requests', 'RequestsController@index')->name('requests');
Route::get('/request/{id}', 'RequestsController@view')->name('request.view');
Route::post('/request/fulfil', 'RequestsController@fulfilment')->name('request.fulfilment');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
