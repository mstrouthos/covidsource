<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestFulfilment extends Model
{
    protected $fillable = ['fulfiled_by_user_id', 'request_item_id', 'qty', 'status', 'reviewed_by_user_id'];
}
