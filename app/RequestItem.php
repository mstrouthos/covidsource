<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestItem extends Model
{
    protected $fillable = ['supplies_request_id', 'item_id', 'qty_requested', 'qty_fulfilled', 'dropoff_user_id', 'state'];

    public function supplies_request(){
      return $this->belongsTo('App\SuppliesRequest');
    }

    public function item(){
      return $this->belongsTo('App\Item');
    }
}
