<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'description', 'image_url', 'useful_resources', 'assembly_instructions_url'];
}
