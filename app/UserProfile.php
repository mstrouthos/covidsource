<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = ['user_id', 'first_name', 'last_name', 'phone_number', 'country_iso'];
}
