<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributionHelper extends Model
{
    protected $fillable = ['user_id', 'distribution_task', 'location_coordinates', 'city'];
}
