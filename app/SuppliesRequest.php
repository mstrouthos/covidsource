<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppliesRequest extends Model
{
    protected $fillable = ['place_name', 'contact_name', 'email', 'phone_number', 'address', 'city', 'notes'];
}
