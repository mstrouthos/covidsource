<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserTool;
use App\Http\Controllers\Controller;
use App\DistributionHelper;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/requests';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'city' => $data['city'],
            'area' => $data['area'],
            'location_coordinates' => $data['location_coordinates'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);



        foreach($data as $key => $value){
          if(strpos($key, "item-") === 0){
            $item_id = intval(substr($key, strlen("item-"), strlen($key)));

            UserTool::create([
              'tool_id' => $item_id,
              'user_id' => $user->id
            ]);
          }else if(strpos($key, "distribution-task-") === 0){
            $distribution_task = substr($key, strlen("distribution-task-"), strlen($key));

            DistributionHelper::create([
              'user_id' => $user->id,
              'location_coordinates' => $data['location_coordinates'],
              'distribution_task' => $distribution_task,
              'city' => $user->city
            ]);
          }
        }



        return $user;
    }
}
