<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuppliesRequest;
use App\RequestItem;
use App\RequestFulfilment;
use Auth;

class RequestsController extends Controller
{
    public function index()
    {
      $requests = RequestItem::where('state', 'approved')->paginate(16);

      if(Auth::check()){
        // get all of the requests for the user's tools and help tasks registered

      }

      return view('ppe.requests')->with(compact('requests'));
    }

    public function view($id){
      $request = RequestItem::findOrFail($id);

      return view('ppe.request')->with(compact('request'));
    }

    public function fulfilment(Request $request){
      $data = $request->all();

      RequestFulfilment::create([
        'fulfiled_by_user_id' => Auth::id(),
        'request_item_id' => $data['request_item_id'],
        'qty' => $data['quantity']
      ]);

      $request_item = RequestItem::find($data['request_item_id']);
      $request_item->qty_fulfilled += $data['quantity'];
      $request_item->save();

      return redirect()->back()->with('message', 'Your submission has been submitted for review. Thank you for your contribution!');
    }

    public function create(Request $request)
    {
      $data = $request->all();

      $supplies_request = new SuppliesRequest();
      $supplies_request->fill($data);
      $supplies_request->save();

      $query_str = "item-qty-";
      foreach($data as $key => $qty){
        if(strpos($key, $query_str) === 0){
          $item_id = intval(substr($key, strlen($query_str), strlen($key)));

          if($qty > 0){
            RequestItem::create([
              'item_id' => $item_id,
              'supplies_request_id' => $supplies_request->id,
              'qty_requested' => $qty
            ]);
          }
        }
      }

      return view('ppe.submission-complete');
    }


}
