<footer id="footer" class="bg-softblack" style="padding-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
              <a href="/">
                <img style="width:100px; height:auto;" src="{{ URL::asset('images/covid.png')}}" alternate="CovidSource Logo"/>
              </a>
            </div><!--/.col-md-3-->

            <div class="col-md-6 col-sm-6">
                <div>
                    <ul class="">
                        <li class="footer-link"><a href="{{ url('/') }}">Home</a></li>
                        <li class="footer-link"><a href="{{ url('about') }}">About</a></li>
                    </ul>
                </div>
            </div><!--/.col-md-3-->
            <div class="col-md-6 col-sm-6">
              <div>
                  <ul class="">
                      <li class="footer-link"><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                      <li class="footer-link"><a href="{{ url('/privacy-policy#cookies-policy') }}">Cookies Policy</a></li>
                      <li class="footer-link"><a href="{{ url('/terms-conditions') }}">Terms and Conditions</a></li>
                  </ul>
              </div>

            </div> <!--/.col-md-3-->
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p style="font-size: 18px; color: #fff;">Copyright 2020</p>
            </div>
        </div>
    </div>
</footer><!--/#bottom-->
