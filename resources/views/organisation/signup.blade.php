@extends('layout.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center">
        <h1>{{ trans('organisation.signup') }}</h1>
        <p>{{ trans('organisation.signup_description')}}</p>
      </div>
    </div>
    <div class="row">
        <form>
          <div class="form-group">
            <label class="col-md-2 col-md-offset-3">Organisation name</label>
            <div class="col-md-4">
              <input class="form-control" name="organisation_name">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 col-md-offset-3">Email Address</label>
            <div class="col-md-4">
              <input class="form-control" name="email_address">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 col-md-offset-3">Contact Name</label>
            <div class="col-md-4">
              <input class="form-control" name="contact_name">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 col-md-offset-3">Phone Number</label>
            <div class="col-md-4">
              <input class="form-control" name="phone_number">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 col-md-offset-3">Address</label>
            <div class="col-md-4">
              <input class="form-control" name="address_line_1">
              <input class="form-control" name="address_line_2">
              <input class="form-control" name="address_postcode">
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-12 text-center">
              <button type="submit" class="btn btn-theme btn-round">{{ trans('organisation.signup') }}</button>
            </div>
          </div>
        </form>
    </div>
  </div>
@endsection
