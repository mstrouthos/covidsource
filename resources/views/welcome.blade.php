@extends('layout.app')

@section('content')
  <div class="content">
    <section class="bg-primary" style=" background-image: url('{{ URL::asset('images/masks.jpg')}}'); background-size:cover; min-height: 500px;">
      <div class="container">
        <div class="row" style="padding-bottom: 5px; padding-top: 50px;">
          <div class="col-md-6 col-xs-12">
            <div class="row white-text">
              <h1>{{ trans('welcome.welcome_title') }}</h1>
              <p style="font-size: 16px; font-weight: 500;">{{ trans('welcome.aim') }}</p>
              <p style="font-size: 16px; font-weight: 500;">{{ trans('welcome.pitch')}}</p>
              <p style="font-size: 16px; font-weight: 500;">{{ trans('welcome.cfa') }}</p>
            </div>
            <div class="row" style="padding-top: 30px;">
              <div class="col-md-6 col-xs-12 text-left">
                <button class="btn btn-theme btn-square"><a href="{{ route('contribute-ppe') }}">{{ trans('welcome.i_can_help')}}</a></button>
              </div>
              <div class="col-md-6 col-xs-12 text-left">
                <button class="btn btn-theme btn-square"><a href="{{ route('need-ppe') }}">{{ trans('welcome.i_need_ppe')}}</a></button>
              </div>
            </div>
          </div>
          <div class="Col-md-6 col-xs-12 text-center">
            <img src="{{ URL::asset('images/covid.png')}}" style="max-width:100%; height: auto">
          </div>
        </div>
      </div>
    </section>


    <section class="bg-primary">
      <div class="container">
        <div class="row" style="padding-top: 50px; padding-bottom: 50px;">
          <div class="col-md-4 text-center">
            <div style="margin: 20px; font-size: 20px;">
              Items Donated
              <span style="font-size: 50px;">730</span>

            </div>
          </div>

          <div class="col-md-4 text-center">
            <div style="margin: 20px; font-size: 20px;">
              Motivated Helpers
              <span style="font-size: 50px;">68</span>

            </div>
          </div>

          <div class="col-md-4 text-center">
            <div style="margin: 20px; font-size: 20px;">
              Fulfilled Requests
              <span style="font-size: 50px;">12</span>

            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Equipment Requests -->
    <section>
      <div class="container">
        <div class="row" style="padding-top: 100px; padding-bottom: 50px;">
          <div class="col-md-12 text-center">
            <h3>Equipment Requests currently open</h3>
          </div>
        </div>
        <div class="row">
          @foreach(App\RequestItem::take(3)->get() as $item_request)
          <div class="col-md-4 text-center">
            <img style="width: 200px; height: 200px;" src="{{ $item_request->item->image_url }}"><br>
            <span style="font-size: 24px;">{{ $item_request->supplies_request->place_name }}</span>
            <p>{{ $item_request->item->name }}</p>
            <p>Qty Requested : {{ $item_request->qty_requested}}</p>
            <p>Qty Fulfilled : {{ $item_request->qty_fulfilled}}</p>
          </div>
          @endforeach
        </div>
        <div class="row" style="padding-top: 50px; padding-bottom: 30px;">
          <div class="col-md-12 text-center">
            <a href="{{ route('requests') }}" class="btn btn-square btn-blue">View all requests</a>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-primary">
      <div class="container">
        <div class="row" style="padding-top: 100px; padding-bottom: 10px;">
          <div class="col-md-12 text-center">
            <h3>Equipment that our makers can provide!</h3>
          </div>
        </div>
        <div class="row" style="padding-bottom: 20px;">
          @foreach(App\Item::limit(12)->get() as $item)
          <div class="col-md-4 text-center">
            <h3>{{ $item->name }}</h3>
            <img style="max-width: 100%; height: auto;" src="{{ $item->image_url }}">
          </div>
          @endforeach
        </div>
      </div>
    </section>
  </div>
@endsection
