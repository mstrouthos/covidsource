@extends('layout.app')

@section('content')
<section class="bg-primary" style=" background-image: url('{{ URL::asset('images/masks.jpg')}}'); background-size:cover; min-height: 500px;">
  <div class="container">
    <div class="row justify-content-center" style="padding-bottom: 5px; padding-top: 200px;">
      <div class="col-md-6 col-md-offset-3 col-xs-12 text-center">
        <h1 style="white-text">Request PPE, our helpers will do their best to keep you safe!</h1>
      </div>
    </div>
  </div>
</section>
<div class="container" style="padding-top: 100px; padding-bottom: 100px;">
  <div style="border: 2px solid #3490dc;">
    <div class="row justify-content-center">
      <div class="col-md-8 col-md-offset-2 text-center">
        <h1>{{ trans('need.request_complete_title') }}</h1>
      </div>
    </div>
    <div class="row justify-content-center" style="padding-top: 50px; padding-bottom: 50px;">
      <div class="col-md-8 col-md-offset-2 text-center">
        <i class="fa fa-check" style="font-size: 80px; color: #3490dc !important" aria-hidden="true"></i>
      </div>
    </div>

    <div class="row" style="padding-top: 50px; padding-bottom: 50px;">
      <div class="col-md-12 text-center">
        <a class="btn" href="{{ route('requests') }}">View All Requests</a>
      </div>
    </div>
  </div>
</div>
@endsection
