@extends('layout.app')

@section('content')
<section class="bg-primary" style=" background-image: url('{{ URL::asset('images/masks.jpg')}}'); background-size:cover; min-height: 500px;">
  <div class="container">
    <div class="row justify-content-center" style="padding-bottom: 5px; padding-top: 200px;">
      <div class="col-md-6 col-md-offset-3 col-xs-12 text-center">
        <h1 class="white-text">Make PPE, and help those in need!</h1>
      </div>
    </div>
  </div>
</section>
<!-- Equipment Requests -->
<section>
  <div class="container">
    <div class="row" style="padding-top: 100px; padding-bottom: 50px;">
      <div class="col-md-12 text-center">
        <h3>Equipment Requests currently open</h3>
      </div>
    </div>
    <div class="row">
      @foreach(App\RequestItem::take(3)->get() as $item_request)
      <div class="col-md-4 text-center">
        <div style="border: 1px solid #000; padding-bottom: 10px; padding-top: 20px;">
          <img style="width: 200px; height: 200px;" src="{{ $item_request->item->image_url }}"><br>
          <span style="font-size: 24px;">{{ $item_request->supplies_request->place_name }}</span>
          <p>{{ $item_request->item->name }}</p>
          <p>Qty Requested : {{ $item_request->qty_requested}}</p>
          <p>Qty Fulfilled : {{ $item_request->qty_fulfilled}}</p>
          <a class="btn btn-square btn-blue" href="{{ route('request.view', $item_request->id ) }}">Details</a>
        </div>
      </div>
      @endforeach
    </div>
    <div class="row" style="padding-top: 50px;">
      <div class="col-md-12 text-center">
        {!! $requests->links() !!}
      </div>
    </div>
  </div>
</section>
@endsection
