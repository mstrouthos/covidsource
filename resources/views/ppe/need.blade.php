@extends('layout.app')

@section('content')
<section class="bg-primary" style=" background-image: url('{{ URL::asset('images/masks.jpg')}}'); background-size:cover; min-height: 500px;">
  <div class="container">
    <div class="row justify-content-center" style="padding-bottom: 5px; padding-top: 200px;">
      <div class="col-md-6 col-md-offset-3 col-xs-12 text-center">
        <h1 class="white-text">Request PPE, our helpers will do their best to keep you safe!</h1>
      </div>
    </div>
  </div>
</section>

  <div class="container">
    <div class="row justify-content-center" style="padding-top: 50px;">
      <div class="col-md-8 col-md-offset-2 text-center">
        <h1>{{ trans('need.request_title') }}</h1>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-6 col-md-offset-3 col-xs-12">
        <form method="POST" action="{{ route('request-ppe') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Hospital / clinic / organisation you are requesting for :</label>
            <input type="text" class="form-control" name="place_name" aria-describedby="name" placeholder="Enter name of hospital, clinic, or organisation">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">This email will be shared with other helpers on the platform for delivery of equipment.</small>
          </div>
          <div class="form-group">
            <label for="name">Contact name:</label>
            <input type="text" class="form-control" name="contact_name" aria-describedby="contactName" placeholder="Enter name of contact at place">
          </div>
          <div class="form-group">
            <label for="phoneNumber">Phone Number</label>
            <input type="text" class="form-control" name="phone_number" aria-describedby="phoneNumber" placeholder="Enter Phone Number">
            <small id="emailHelp" class="form-text text-muted">The phone number will not be shared, only used by admins in case we need to contact you.</small>
          </div>
          <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" name="address" aria-describedby="address" placeholder="Enter address for dropoff">
          </div>
          <div class="form-group">
            <label for="address">City</label>
            <input type="text" class="form-control" name="city" aria-describedby="city" placeholder="Enter city">
          </div>

          <div class="form-group">
            <label for="equipment">What kind of equipment do you need?</label>

          </div>

          <div class="form-check">
            @foreach(App\Item::all() as $item)
            <div class="col-md-9">
              <input class="form-check-input" type="checkbox" value="" id="item-{{ $item->id }}">
              <label class="form-check-label" for="item-{{ $item->id }}">
                {{ $item->name }}
              </label>
            </div>
            <div class="col-md-3">
              <input name="item-qty-{{ $item->id }}" class="form-control" type="number" min=0 max=1000 value=0>
            </div>
            @endforeach
          </div>

          <div class="form-group">
            <label for="notes">Any other notes regarding the request, or delivery instructions etc</label>
            <input type="text" class="form-control" name="notes" aria-describedby="notes" placeholder="Enter any notes required">
          </div>

          <button type="submit" class="btn btn-primary">Submit Request</button>
        </form>
      </div>
    </div>
  </div>
@endsection
