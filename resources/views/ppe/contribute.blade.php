@extends('layout.app')

@section('content')
<section class="bg-primary" style=" background-image: url('{{ URL::asset('images/masks.jpg')}}'); background-size:cover; min-height: 500px;">
  <div class="container">
    <div class="row justify-content-center" style="padding-bottom: 5px; padding-top: 200px;">
      <div class="col-md-6 col-md-offset-3 col-xs-12 text-center">
        <h1 class="white-text">{{ trans('contribute.make_ppe_help') }}</h1>
      </div>
    </div>
  </div>
</section>
  <div class="container" style="padding-bottom: 50px;">
    <div class="row justify-content-center" style="padding-top: 40px;">
      <div class="col-md-8 col-md-offset-2 text-center">
        <h1>{{ trans('contribute.have_machinery') }}</h1>
        <p>{{ trans('contribute.have_machinery_desc') }}</p>
      </div>
    </div>

    <form method="POST" action="{{ route('register') }}">
      @csrf
      <div class="form-group row justify-content-center">
        <div class="col-md-6 col-md-offset-3 text-left">
            @foreach(App\Tool::all() as $tool)
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="item-{{ $tool->id }}">
              <label class="form-check-label" for="item-{{ $tool->id }}">
                {{ $tool->name }}
              </label>
            </div>
            @endforeach
        </div>
      </div>

      <div class="form-group row justify-content-center">
        <div class="col-md-6 col-md-offset-3 text-left">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="distribution-task-dropoff">
            <label class="form-check-label" for="distribution-task-dropoff">
              Dropoff Point
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="distribution-task-delivery">
            <label class="form-check-label" for="distribution-task-delivery">
              Delivery of Equipment
            </label>
          </div>
        </div>
      </div>

      <div class="form-group row justify-content-center" style="padding-top: 50px;">
        <div class="col-md-6 col-md-offset-3">
          <label>{{ trans('contribute.email') }}</label>
          <input type="email" name="email" class="form-control">
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
      </div>

      <div class="form-group row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
          <label>{{ trans('contribute.name') }}</label>
          <input type="text" name="name" class="form-control">
          @error('username')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
      </div>

      <div class="form-group row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
          <label>{{ trans('contribute.username') }}</label>
          <input type="text" name="username" class="form-control">
          @error('username')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
          <label>{{ trans('contribute.city') }}</label>
          <select class="form-control" name="city">
            <option value="Nicosia">Nicosia</option>
            <option value="Nicosia">Larnaka</option>
            <option value="Nicosia">Limasol</option>
            <option value="Nicosia">Paphos</option>
            <option value="Nicosia">Famagusta</option>
          </select>
          @error('city')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
      </div>


      <div class="form-group row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
          <label>{{ trans('contribute.area') }}</label>
          <input type="text" name="area" class="form-control">
          @error('area')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
      </div>
      <div class="form-group row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
          <label>{{ trans('contribute.password') }}</label>
          <input type="password" name="password" class="form-control">
          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
      </div>

      <input type="hidden" name="location_coordinates" value="0,0">

      <div class="form-group row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
          <label>{{ trans('contribute.confirm_password') }}</label>
          <input type="password" name="password_confirmation" class="form-control">
        </div>
      </div>

      <div class="row justify-content-center" style="padding-top: 20px;">
        <div class="col-md-8 col-md-offset-2 text-center">
          <button type="submit" class="btn btn-blue btn-square">{{ trans('contribute.signup_button') }}</button>
        </div>
      </div>
    </form>
  </div>
@endsection
