@extends('layout.app')

@section('content')
<section class="bg-primary" style=" background-image: url('{{ URL::asset('images/masks.jpg')}}'); background-size:cover; min-height: 500px;">
  <div class="container">
    <div class="row justify-content-center" style="padding-bottom: 5px; padding-top: 200px;">
      <div class="col-md-6 col-md-offset-3 col-xs-12 text-center">
        <h1 class="white-text">Make PPE, and help those in need!</h1>
      </div>
    </div>
  </div>
</section>
<!-- Equipment Requests -->
<section>
  <div class="container">
    <div class="row" style="padding-top: 100px; padding-bottom: 50px;">
      <div class="col-md-12 text-center">
        <h3>Equipment Request from {{ $request->supplies_request->place_name }}</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h3>Request item : {{ $request->item->name }}</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <img src="{{ $request->item->image_url }}" style="width:100%; height: auto;">
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-12">
            <span style="font-size: 20px;">Quantity Requested: <strong>{{ $request->qty_requested }}</strong></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <span  style="font-size: 20px;">Quantity Fulfilled: <strong>{{ $request->qty_fulfilled }}</strong></span>
          </div>
        </div>

        <div class="row" style="padding-top: 20px; padding-bottom: 50px;">
          <div class="col-md-12">
            <span>Dropoff Location</span>
          </div>
          <div class="col-md-12">
            <div id="map" style="height: 300px; width:100%;">
            </div>
          </div>
        </div>

        @if(Auth::check())
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <form method="POST" action="{{ route('request.fulfilment')}}">
          <div class="form-group row">
            @csrf
            <input type="hidden" name="request_item_id" value="{{ $request->id }}">
            <label class="col-md-6 control-label" for="qty">Deliver Quantity</label>
            <div class="col-md-6">
              <input type="number" class="form-control" required name="quantity" min=1 max="{{ $request->qty_requested - $request->qty_fulfilled }}" value=5>
            </div>

          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <button class="btn btn-primary" type="submit">Send</button>
            </div>
          </div>
        </form>
        @else
          <span> Want to help? <a href="{{ route('contribute-ppe') }}">Register</a> and get started!</span>
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <h3>Useful Resources</h3>

        <p>{{ $request->item->useful_resources }}</p>
      </div>
    </div>

    <div class="row" style="padding-bottom: 20px;">
      <div class="col-md-12">
        <a target="_blank" href="{{ $request->item->assembly_instructions_url }}">Assembly Instructions</h3>
      </div>
    </div>


  </div>
</section>
@endsection

@section('scripts')
  <script type="text/javascript">
      function initMap() {
        var dropoffLocation = {lat: 35.1419328, lng: 33.318893};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 18, center: dropoffLocation});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: dropoffLocation, map: map});
      }


      </script>
      <script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMPmR62cXYQuTJ5-PyPeYYoxZj6f2LjTU&callback=initMap">
   </script>
@endsection
