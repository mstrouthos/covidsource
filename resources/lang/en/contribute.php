<?php
  // English

  return [
    'make_ppe_help' => 'Make PPE, and help those in need!',

    'live_in' => "I live in ...",
    'live_in_desc' => "Due to the widespread lockdown restrictions you will only see local supply requests where it's easy to send/deliver the supplies to.",
    'country_select' => "Please select ...",
    'email' => 'Email Address',
    'have_machinery' => "I have a ...",
    'have_machinery_desc' => 'You can select all three if you have them, this will affect the projects you see in your dashboard',
    'username' => "Username (visible to other users)",
    'password' => "Password (secret)",
    'confirm_password' => "Confirm your password",
    'signup_button' => "Signup and contribute",
    'name' => 'Full Name',
    'city' => 'City',
    'area' => 'Area',



    // Machinery
    'sewing_machine' => 'Sewing Machine',
    'laser_cutter' => 'Laser Cutter',
    '3d_printer' => '3D Printer',


  ];

 ?>
