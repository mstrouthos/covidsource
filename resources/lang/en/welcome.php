<?php

return [

  'i_can_help' => 'I can help',
  'i_need_ppe' => 'I need PPE',
  'signup' => 'Signup',
  'welcome_title' => 'Welcome to CovidSource',
  'aim' => 'Our aim is to connect the maker-crowd with those in need of medical supplies for the Covid outbreak',
  'pitch' => 'Have a 3D printer, laser cutter, sewing machine or any other machine that you think can produce much needed supplies?',
  'cfa' => 'Stop wasting time and click on the button below to get started contributing!',

  'organisation_text' => 'Have an organisation and would like to request equipment? Sign up',
  'here' => 'here',

];
