<?php

return [
  'signup' => 'Organisation Signup',
  'signup_description' => 'If you belong or manage an organisation signup the organisation here to get approved for requesting supplies to be made and shipped to your organisation per request. You can request quantities of specific items and your request will be kept alive until the quantity is fulfilled or until you close it.',
  'organisation_name' => 'Organisation Name',
  'email' => 'Email Address',
  'contact_name' => 'Contact Name',
  'phone_number' => 'Phone Nubmer',
  'address' => 'Address',
];
