<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('supplies_request_id');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('dropoff_user_id')->nullable();
            $table->unsignedInteger('qty_requested');
            $table->unsignedInteger('qty_fulfilled')->default(0);
            $table->enum('state', ['pending', 'approved', 'declined', 'fulfilled'])->default('pending');
            $table->timestamps();

            $table->foreign('supplies_request_id')->references('id')->on('supplies_requests');
            $table->foreign('dropoff_user_id')->references('id')->on('users');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_items');
    }
}
