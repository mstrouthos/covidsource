<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestFulfilmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_fulfilments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('fulfiled_by_user_id');
            $table->unsignedBigInteger('request_item_id');
            $table->unsignedInteger('qty');
            $table->enum('status', ['pending', 'accepted', 'declined'])->default('pending');
            $table->unsignedBigInteger('reviewed_by_user_id')->nullable();
            $table->unsignedBigInteger('dropoff_point_id');
            $table->timestamps();

            $table->foreign('fulfiled_by_user_id')->references('id')->on('users');
            $table->foreign('request_item_id')->references('id')->on('request_items');
            $table->foreign('reviewed_by_user_id')->references('id')->on('users');
            $table->foreign('dropoff_point_id')->references('id')->on('distribution_helpers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_fulfilments');
    }
}
